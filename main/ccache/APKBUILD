# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=ccache
pkgver=4.5.1
pkgrel=0
pkgdesc="fast C/C++ compiler cache"
url="https://ccache.dev/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	asciidoctor
	cmake
	hiredis-dev
	linux-headers
	perl
	samurai
	zstd-dev
	"
checkdepends="bash util-linux-misc python3 redis"
subpackages="$pkgname-doc"
source="https://github.com/ccache/ccache/releases/download/v$pkgver/ccache-$pkgver.tar.xz
	ioctl.patch"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	case $CARCH in
		armhf) echo "SKIP make -C build test on $CARCH" ;;
		*) 	ninja -C build test ;;
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	local link=
	mkdir -p "$pkgdir"/usr/lib/ccache/bin

	for link in cc gcc g++ cpp c++ $CHOST-cc $CHOST-gcc \
			$CHOST-g++ $CHOST-cpp $CHOST-c++ \
			clang clang++; do
		ln -sf /usr/bin/ccache "$pkgdir"/usr/lib/ccache/bin/$link
	done
}

sha512sums="
267dcc6b41270eeffe029d13e58eca3399540037cc19dc58bb5ebeb7dcc51b201fbde91c9824eaee5b14fbf28bb7304b78d1340118bc72e56b80ff148575bc56  ccache-4.5.1.tar.xz
785ce34305a3bb6c24117341157356c2bd2272eca9d58fa20acd14a023abe6f784c88d9c55656d2f37320392bb73a61c52cd8b6bd9ac7c5316c8ed187dd6f5fb  ioctl.patch
"
