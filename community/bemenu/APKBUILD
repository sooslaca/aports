# Contributor: Cosmo Borsky <me@cosmoborsky.com>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bemenu
pkgver=0.6.6
pkgrel=0
pkgdesc="Dynamic menu library and client program with support for different backends"
options="!check" # No testsuite
url="https://github.com/Cloudef/bemenu"
arch="all"
license="GPL-3.0-or-later AND LGPL-3.0-or-later"
depends_dev="
	libxinerama-dev
	libxkbcommon-dev
	ncurses-dev
	pango-dev
	scdoc
	wayland-dev
	wayland-protocols
	"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
source="$pkgname-$pkgver.tar.gz::https://github.com/Cloudef/bemenu/archive/$pkgver.tar.gz"

build() {
	PREFIX=/usr CFLAGS="$CFLAGS -g" make
}

package() {
	# Please don't split backends into subpackages until
	# https://github.com/Cloudef/bemenu/issues/165 is
	# resolved, i.e. proper error messages are output if
	# no backend was found or a backend error occured.

	make PREFIX=/usr DESTDIR="$pkgdir" install
}

sha512sums="
28cf62a2bcb030fa7c99403754edf35736d7df1f0d55e5277af00a3f279c98c8af5143339fa9ed7b29447631d9e7a4f773ec599801a898390fa94ed8e6b03eb0  bemenu-0.6.6.tar.gz
"
