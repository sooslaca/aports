# Contributor: André Klitzing <aklitzing@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=ostree
pkgver=2022.1
pkgrel=0
pkgdesc="Operating system and container binary deployment and upgrades"
url="https://github.com/ostreedev/ostree"
arch="all"
license="LGPL-2.0-or-later"
makedepends="bison xz-dev libarchive-dev e2fsprogs-dev
	libsoup-dev gpgme-dev fuse-dev linux-headers gtk-doc libxslt
	automake autoconf libtool curl-dev"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-dbg
	$pkgname-grub
	$pkgname-bash-completion:bashcomp:noarch
	"
source="https://github.com/ostreedev/ostree/releases/download/v$pkgver/libostree-$pkgver.tar.xz
	musl-fixes.patch
	"
options="!check"
builddir="$srcdir/lib$pkgname-$pkgver"

prepare() {
	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--sbindir=/usr/bin \
		--libexecdir=/usr/lib \
		--enable-gtk-doc \
		--disable-static \
		--disable-glibtest \
		--with-curl \
		--with-openssl
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	amove usr/lib/libostree/ostree-trivial-httpd
}

grub() {
	pkgdesc="GRUB2 integration for OSTree"
	install_if="$pkgname=$pkgver-r$pkgrel grub"

	amove etc/grub.d/*
	amove usr/lib/libostree/grub2-*
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	amove usr/share/bash-completion
}

sha512sums="
73da367d3b9b5facc5ba894bd0f9297a7fc688fa69871b668e6568663fefc1f44433c8be352d57c23153b09144c6535e52d8c184a2ab103190ec37dc1abc2818  libostree-2022.1.tar.xz
affde3a0a1c27066c2fcc30212343274de9b3856e913adc340afa4fbfb6398732c4c39dee52837459a77273015aec4e98ea61c2b5e3e884b795462485657933c  musl-fixes.patch
"
