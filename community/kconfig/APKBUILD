# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfig
pkgver=5.91.0
pkgrel=0
pkgdesc="Configuration system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfig-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E 'kconfig(core-(kconfig|kdesktopfile)|gui-kstandardshortcutwatcher)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e1dccb4eab9d5969ee9c53a81d4170388337d606ee4014fbb70f669efa6118fb8dd849cd93fa6bcfd0025a60302a335e79780dc908379d507c3a1a5280603a58  kconfig-5.91.0.tar.xz
"
