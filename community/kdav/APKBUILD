# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdav
pkgver=5.91.0
pkgrel=0
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
license="LGPL-2.0-or-later"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	qt5-qtxmlpatterns-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
11319f7bac3c4233cdbfb59980105c241a41d0d032edd10a9392832d7ae348d051bae8b5a0f4f18c8bb1321635b81c05c58ca2130923632377969abc0980b7e6  kdav-5.91.0.tar.xz
"
