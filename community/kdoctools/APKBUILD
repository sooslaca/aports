# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdoctools
pkgver=5.91.0
pkgrel=0
pkgdesc="Documentation generation from docbook"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="docbook-xml docbook-xsl"
depends_dev="qt5-qtbase-dev ki18n-dev karchive-dev libxslt-dev libxml2-dev libxml2-utils"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz perl-uri qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdoctools-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
3ea401584e92c300ef65ab9c18fc8da8333bdd569d8fe226f15fbcb7aa401c109532783bf1319369e9966bb38c7f32d9fdb7aef17b065cd3d6828cda56bee968  kdoctools-5.91.0.tar.xz
"
