# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesignerplugin
pkgver=5.91.0
pkgrel=0
pkgdesc="Integration of Frameworks widgets in Qt Designer/Creator"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kplotting-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ca5e648409c03178e79ba707501f7a39cb4664abeea4de9b5c301451787fc7bca597b902a8af193a00a892eae2b34c9751082cb398ec204e294b424a9237772a  kdesignerplugin-5.91.0.tar.xz
"
