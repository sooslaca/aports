# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.21.6
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="rust cargo dbus-dev openssl-dev libpulse lm-sensors-dev"
options="net !check" # no tests
provides="i3status-rs"
subpackages="$pkgname-doc"
source="https://github.com/greshake/i3status-rust/archive/v$pkgver/i3status-rust-$pkgver.tar.gz"

build() {
	cargo build --release --verbose --locked
}

package() {
	install -Dm755 target/release/i3status-rs "$pkgdir"/usr/bin/i3status-rs

	install -Dm644 man/i3status-rs.1 "$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/themes files/themes/*
	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/icons files/icons/*
}

sha512sums="
fbef7b7c62225fbc1946bf5024cc38bd770324f614a0f8f86a9220c45892d99a5cb67dd91d3ae328719106d63bbc385e0f4bc7426461ea57285dba9d102fadb9  i3status-rust-0.21.6.tar.gz
"
