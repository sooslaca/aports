# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=grype
pkgver=0.33.0
pkgrel=0
pkgdesc="Vulnerability scanner for container images, filesystems, and SBOMs"
url="https://github.com/anchore/grype"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/grype/archive/v$pkgver/grype-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -o bin/grype
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/grype "$pkgdir"/usr/bin/grype
}

sha512sums="
4db0c0e2b0ebc8cffedb46a399414fbba71484b359910dd3458fdf9d29e90ea7f8b0fdcd2e9fc8930c5493e4e8573a739d868cfb16488c7921c94fcef1d767cf  grype-0.33.0.tar.gz
"
