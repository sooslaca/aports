# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
_pkgver="2022-02-14"
pkgver=${_pkgver//-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-analyzer/rust-analyzer"
# limited by rust, x86/armhf/armv7 fail tests
arch="x86_64 aarch64 ppc64le"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rust-analyzer/rust-analyzer/archive/refs/tags/$_pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin target/release/rust-analyzer

	install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE-MIT LICENSE-APACHE
	install -Dm644 docs/user/manual.adoc "$pkgdir"/usr/share/doc/$pkgname/manual.adoc
}

sha512sums="
cad62bc2837daadf17fadd3e4ccb2cc9ea4203f14cdac6931548a025b2cd5daef5f4b645d36381fea5a5baf44b301d8b83f0136c095dcc3b500df476baab6f1b  rust-analyzer-20220214.tar.gz
"
