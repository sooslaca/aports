# Contributor: Michael Pirogov <vbnet.ru@gmail.com>
# Maintainer: Michael Pirogov <vbnet.ru@gmail.com>
pkgname=postgresql-citus
_projname=citus
pkgver=10.2.4
pkgrel=0
pkgdesc="Scalable PostgreSQL for multi-tenant and real-time analytics workloads"
url="https://github.com/citusdata/citus"
arch="all"
license="AGPL-3.0-only"
makedepends="autoconf postgresql-dev icu-dev lz4-dev zstd-dev"
install="$pkgname.post-install"
source="https://github.com/citusdata/citus/archive/v$pkgver/$_projname-$pkgver.tar.gz"
builddir="$srcdir/$_projname-$pkgver"
options="!check" # tests requires running postgresql cluster

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--prefix=/usr \
		--without-libcurl
	make
}

package() {
	depends="postgresql$(pg_config --major-version)"

	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
f9d6853333984c23b3f6aaeec156719c8564958e4d446781506f95fd56e48b8df27f1ddfa1cddb085791dccd6a17c07f3df87d84a89e137ba5a6972a656222f2  citus-10.2.4.tar.gz
"
